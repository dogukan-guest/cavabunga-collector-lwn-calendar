package edu.itu.cavabunga.cavabungacollectorlwncalendar.parser;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.exception.CollectorException;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.http.HttpAdapter;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.LwnObjectMapper;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMContinue;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMDate;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMEvent;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory.CalMContinueFactory;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory.CalMDateFactory;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory.CalMEventFactory;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LwnCalendarParser {
    private HttpAdapter httpAdapter;
    private CalMContinueFactory calMContinueFactory;
    private CalMDateFactory calMDateFactory;
    private CalMEventFactory calMEventFactory;
    private LwnObjectMapper lwnObjectMapper;

    private String baseLwnCalendarUrl;

    @Autowired
    public LwnCalendarParser(HttpAdapter httpAdapter,
                             CalMContinueFactory calMContinueFactory,
                             CalMEventFactory calMEventFactory,
                             CalMDateFactory calMDateFactory,
                             LwnObjectMapper lwnObjectMapper){
        this.httpAdapter = httpAdapter;
        this.calMContinueFactory = calMContinueFactory;
        this.calMEventFactory = calMEventFactory;
        this.calMDateFactory = calMDateFactory;
        this.lwnObjectMapper = lwnObjectMapper;
        this.baseLwnCalendarUrl = "https://lwn.net/Calendar/Monthly/";
    }

    public List<CalMDate> parseCalendarForDate(String year, String month){
       String calendarUrl = this.baseLwnCalendarUrl + year + "-" + month + "/";
       List<CalMDate> result = new ArrayList<>();
       try {
           org.jsoup.nodes.Document doc = Jsoup.connect(calendarUrl).get();
           org.jsoup.select.Elements rows = doc.select("tr");
           Integer rowCount = 1;
           Integer columnCount = 1;
           Integer maxColumnCount = 8;
           for (org.jsoup.nodes.Element row : rows) {
               org.jsoup.select.Elements columns = row.select("td");
               for (org.jsoup.nodes.Element column : columns) {
                   if (column.className().equals("CalMDate") && !column.text().trim().equals("")) {
                       CalMDate date = calMDateFactory.create();
                       if(Integer.parseInt(column.text()) < 10 ){
                           date.setText("0" + column.text());
                       }else{
                           date.setText(column.text());
                       }
                       date.setColumn(columnCount);
                       date.setRow(rowCount);
                       date.setDate(year + month + date.getText());
                       System.out.println(date.getDate());
                       result.add(date);
                   }

                   if(column.className().equals("CalBlank") ||
                           column.className().equals("CalMDate") ||
                           column.className().equals("CalMEvent") ||
                           column.className().equals("CalMContinue")){
                       columnCount++;
                   }

                   if (columnCount.equals(maxColumnCount)){
                       columnCount = 1;
                   }
               }

               if (row.className().equals("CalM")) {
                   rowCount++;
               }
               columnCount = 1;
           }
       }catch (Exception e){
           throw new CollectorException("There is something went wrong while parsing calendar data: " + e.getMessage());
       }

       return result;
    }

    public List<CalMEvent> parseCalendarForEvent(String year, String month){
        String calendarUrl = this.baseLwnCalendarUrl + year + "-" + month + "/";
        List<CalMEvent> result = new ArrayList<>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(calendarUrl).get();
            org.jsoup.select.Elements rows = doc.select("tr");
            Integer rowCount = 1;
            Integer columnCount = 1;
            Integer maxColumnCount = 8;
            for (org.jsoup.nodes.Element row : rows) {
                org.jsoup.select.Elements columns = row.select("td");
                for (org.jsoup.nodes.Element column : columns) {
                    if (column.className().equals("CalMEvent") && !column.text().trim().equals("")) {
                        CalMEvent event = calMEventFactory.create();
                        event.setText(column.text());
                        event.setEventUrl(column.select("a").attr("abs:href"));
                        event.setColumn(columnCount);
                        event.setRow(rowCount);
                        System.out.println(event.getRow() + " " + event.getColumn() + " " + event.getText());
                        result.add(event);
                    }

                    if(column.className().equals("CalBlank") ||
                            column.className().equals("CalMDate") ||
                            column.className().equals("CalMEvent") ||
                            column.className().equals("CalMContinue")){
                        columnCount++;
                    }

                    if (columnCount.equals(maxColumnCount)){
                        columnCount = 1;
                    }
                }

                if (row.className().equals("CalM")) {
                    rowCount++;
                }
                columnCount = 1;
            }
        }catch (Exception e){
            throw new CollectorException("There is something went wrong while parsing calendar data: " + e.getMessage());
        }

        return result;
    }

    public List<CalMContinue> parseCalendarForContinue(String year, String month){
        String calendarUrl = this.baseLwnCalendarUrl + year + "-" + month + "/";
        List<CalMContinue> result = new ArrayList<>();
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(calendarUrl).get();
            org.jsoup.select.Elements rows = doc.select("tr");
            Integer rowCount = 1;
            Integer columnCount = 1;
            Integer maxColumnCount = 8;
            for (org.jsoup.nodes.Element row : rows) {
                org.jsoup.select.Elements columns = row.select("td");
                for (org.jsoup.nodes.Element column : columns) {
                    if (column.className().equals("CalMContinue")) {
                        CalMContinue mContinue = calMContinueFactory.create();
                        mContinue.setText(column.text());
                        mContinue.setColumn(columnCount);
                        mContinue.setRow(rowCount);
                        System.out.println(mContinue.getRow() + " " + mContinue.getColumn() + " " + mContinue.getText());
                        result.add(mContinue);
                    }

                    if(column.className().equals("CalBlank") ||
                            column.className().equals("CalMDate") ||
                            column.className().equals("CalMEvent") ||
                            column.className().equals("CalMContinue")){
                        columnCount++;
                    }

                    if (columnCount.equals(maxColumnCount)){
                        columnCount = 1;
                    }
                }

                if (row.className().equals("CalM")) {
                    rowCount++;
                }
                columnCount = 1;
            }
        }catch (Exception e){
            throw new CollectorException("There is something went wrong while parsing calendar data: " + e.getMessage());
        }

        return result;
    }

    public List<CalMEvent> parseCalendar(List<CalMDate> calMDateList, List<CalMEvent> calMEventList){
        List<CalMEvent> result = new ArrayList<>();
        for(CalMDate date : calMDateList){
            for(CalMEvent event : calMEventList){
                if(event.getColumn().equals(date.getColumn()) && event.getRow() > date.getRow()){
                    event.setEventDate(date.getDate());
                    result.add(event);
                }
            }
        }

        return result;
    }
}
