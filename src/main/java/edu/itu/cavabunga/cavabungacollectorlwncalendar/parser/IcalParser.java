package edu.itu.cavabunga.cavabungacollectorlwncalendar.parser;

import edu.itu.cavabunga.lib.entity.Parameter;
import edu.itu.cavabunga.lib.entity.Property;
import org.springframework.stereotype.Component;

@Component
public class IcalParser {
    public String parseComponentToIcal(edu.itu.cavabunga.lib.entity.Component component){
        String result = "BEGIN:V" + component.getClass().getSimpleName().toUpperCase() + "\n";
        if(component.getProperties() != null && !component.getProperties().isEmpty()){
            for(Property p: component.getProperties()){
                result = result + parsePropertyToIcal(p);
            }
        }

        if(component.getComponents() != null && !component.getComponents().isEmpty()){
            for(edu.itu.cavabunga.lib.entity.Component c : component.getComponents()){
                result = result + parseComponentPropertyToIcal(c);
            }
        }

        result = result + "END:V" + component.getClass().getSimpleName().toUpperCase() + "\n";

        return result;
    }

    protected String parseComponentPropertyToIcal(edu.itu.cavabunga.lib.entity.Component component){
        String result = "BEGIN:V" + component.getClass().getSimpleName().toUpperCase() + "\n";
        if(component.getProperties() != null && !component.getProperties().isEmpty()){
            for(Property t: component.getProperties()){
                result = result + parsePropertyToIcal(t);
            }
        }
        result = result + "END:V" + component.getClass().getSimpleName().toUpperCase() + "\n";
        System.out.println(result);
        return result;
    }

    public String parsePropertyToIcal(Property property){
        String parameters = new String() ;
        if(property.getParameters() != null && !property.getParameters().isEmpty()){
            for(Parameter parameter: property.getParameters()){
                parameters = parameters + ";" + parseParemeterToIcal(parameter);
            }
        }
        System.out.println(property.getClass().getSimpleName().toUpperCase() + parameters + ":" + property.getValue());
        return property.getClass().getSimpleName().toUpperCase() + parameters + ":" + property.getValue() + "\n";
    }

    public String parseParemeterToIcal(Parameter parameter){
        System.out.print(parameter.getClass().getSimpleName().toUpperCase() + "=" + parameter.getValue());
        return parameter.getClass().getSimpleName().toUpperCase() + "=" + parameter.getValue();
    }
}
