package edu.itu.cavabunga.cavabungacollectorlwncalendar.http;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.exception.CollectorException;
import org.springframework.stereotype.Component;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class HttpAdapter {
    public String doRequest(String requestUrl) {
        try {
            URL url = new URL(requestUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            return content.toString();
        } catch (Exception e) {
            throw new CollectorException("There is something wrong with http connection: " + e.getMessage());
        }
    }
}
