package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMEvent;
import org.springframework.stereotype.Component;

@Component
public class CalMEventFactoryImp implements CalMEventFactory {
    public CalMEventFactoryImp(){}

    public CalMEvent create(){
        return new CalMEvent();
    }

}
