package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element;

import lombok.Data;

@Data
public abstract class Element {
    private Integer row;
    private Integer column;
    private String text;
}
