package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element;

import lombok.Data;

import java.util.Date;

@Data
public class CalMEvent extends Element{
    private String eventDate;
    private String eventUrl;
}
