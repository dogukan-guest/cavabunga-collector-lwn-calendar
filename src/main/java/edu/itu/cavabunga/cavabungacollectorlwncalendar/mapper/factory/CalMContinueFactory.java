package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMContinue;

public interface CalMContinueFactory {
    CalMContinue create();
}
