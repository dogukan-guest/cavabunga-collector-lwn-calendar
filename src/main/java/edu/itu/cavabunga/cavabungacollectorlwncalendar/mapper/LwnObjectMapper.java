package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMEvent;
import edu.itu.cavabunga.lib.entity.Parameter;
import edu.itu.cavabunga.lib.entity.Property;
import edu.itu.cavabunga.lib.entity.component.ComponentType;
import edu.itu.cavabunga.lib.entity.parameter.ParameterType;
import edu.itu.cavabunga.lib.entity.property.PropertyType;
import edu.itu.cavabunga.lib.factory.ParameterFactory;
import edu.itu.cavabunga.lib.factory.ParameterFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class LwnObjectMapper {
    private edu.itu.cavabunga.lib.factory.ComponentFactory componentFactory;
    private edu.itu.cavabunga.lib.factory.PropertyFactory propertyFactory;
    private ParameterFactory parameterFactory;

    @Autowired
    public LwnObjectMapper(edu.itu.cavabunga.lib.factory.ComponentFactoryImpl componentFactory,
                           edu.itu.cavabunga.lib.factory.PropertyFactoryImpl propertyFactory,
                           ParameterFactoryImpl parameterFactory){
        this.componentFactory = componentFactory;
        this.propertyFactory = propertyFactory;
        this.parameterFactory = parameterFactory;
    }

    public edu.itu.cavabunga.lib.entity.Component mapEventsToCoreObject(List<CalMEvent> calMEventList){
        edu.itu.cavabunga.lib.entity.Component calendar = componentFactory.createComponent(ComponentType.Calendar);
        Property version = propertyFactory.createProperty(PropertyType.Version);
            version.setValue("2.0");
        Property prodid = propertyFactory.createProperty(PropertyType.Prodid);
            prodid.setValue("-//CAVABUNGA Open calendar server//");
        Property calscale = propertyFactory.createProperty(PropertyType.Calscale);
            calscale.setValue("GREGORIAN");
        calendar.addProperty(version);
        calendar.addProperty(prodid);
        calendar.addProperty(calscale);

        for(CalMEvent calMEvent : calMEventList){
            edu.itu.cavabunga.lib.entity.Component event = componentFactory.createComponent(ComponentType.Event);
            Property summary = propertyFactory.createProperty(PropertyType.Summary);
                summary.setValue(calMEvent.getText());
            Property url = propertyFactory.createProperty(PropertyType.Url);
                url.setValue(calMEvent.getEventUrl());
            Property dtstart = propertyFactory.createProperty(PropertyType.Dtstart);
                dtstart.setValue(calMEvent.getEventDate());
            Parameter value = parameterFactory.createParameter(ParameterType.Value);
                value.setValue("DATE");
            dtstart.addParameter(value);
            Property dtend = propertyFactory.createProperty(PropertyType.Dtend);
                dtend.setValue(calMEvent.getEventDate());
            dtend.addParameter(value);

            event.addProperty(summary);
            event.addProperty(url);
            event.addProperty(dtstart);
            event.addProperty(dtend);

            calendar.addComponent(event);
        }

        return calendar;
    }
}
