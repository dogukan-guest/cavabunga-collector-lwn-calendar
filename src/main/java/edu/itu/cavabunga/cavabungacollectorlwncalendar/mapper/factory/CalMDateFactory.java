package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMDate;

public interface CalMDateFactory {
    CalMDate create();
}
