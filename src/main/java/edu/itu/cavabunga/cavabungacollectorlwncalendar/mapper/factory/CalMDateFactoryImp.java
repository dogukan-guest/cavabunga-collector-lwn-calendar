package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMDate;
import org.springframework.stereotype.Component;

@Component
public class CalMDateFactoryImp implements CalMDateFactory {
    public CalMDateFactoryImp(){}

    public CalMDate create(){
        return new CalMDate();
    }

}
