package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element;

import lombok.Data;

@Data
public class CalMDate extends Element{
    private String date;
}
