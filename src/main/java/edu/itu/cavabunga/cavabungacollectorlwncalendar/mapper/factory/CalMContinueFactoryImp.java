package edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.factory;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMContinue;
import org.springframework.stereotype.Component;

@Component
public class CalMContinueFactoryImp implements CalMContinueFactory {
    public CalMContinueFactoryImp(){}

    public CalMContinue create(){
        return new CalMContinue();
    }
}
