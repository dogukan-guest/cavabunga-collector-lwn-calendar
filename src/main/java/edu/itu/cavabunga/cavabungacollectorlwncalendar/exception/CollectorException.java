package edu.itu.cavabunga.cavabungacollectorlwncalendar.exception;

public class CollectorException extends RuntimeException {
    public CollectorException(){}
    public CollectorException(String message){
        super(message);
    }
}
