package edu.itu.cavabunga.cavabungacollectorlwncalendar.controller;

import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.LwnObjectMapper;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMContinue;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMDate;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.mapper.element.CalMEvent;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.parser.IcalParser;
import edu.itu.cavabunga.cavabungacollectorlwncalendar.parser.LwnCalendarParser;
import edu.itu.cavabunga.lib.entity.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
public class MainController {
    @Autowired
    private LwnCalendarParser lwnCalendarParser;

    @Autowired
    private LwnObjectMapper lwnObjectMapper;

    @Autowired
    private IcalParser icalParser;

    @GetMapping(value = "ics/{year}/{month}", produces = "text/calendar")
    public String parseToIcs(@PathVariable(value = "year") String year, @PathVariable(value="month") String month){
        return icalParser.parseComponentToIcal(lwnObjectMapper.mapEventsToCoreObject(lwnCalendarParser.parseCalendar(lwnCalendarParser.parseCalendarForDate("2018","07"),
                lwnCalendarParser.parseCalendarForEvent(year,month))));
    }

    @GetMapping(value = "json/{year}/{month}", produces = "application/json")
    public Component parseToJson(@PathVariable(value = "year") String year, @PathVariable(value="month") String month){
        return lwnObjectMapper.mapEventsToCoreObject(lwnCalendarParser.parseCalendar(lwnCalendarParser.parseCalendarForDate("2018","07"),
                lwnCalendarParser.parseCalendarForEvent(year,month)));
    }
}
